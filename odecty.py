from datetime import datetime
from criteria import readConfigurationFile

class ConfigurationInfo:
    '''
    object of configuration information
    '''
    magnitude = "undefined"
    depthMin = -1
    depthMax = -1
    latitude1 = -1
    longitude1 = -1
    latitude2 = -1
    longitude2 = -1
    deploymentP = -1
    deploymentS = -1
    outputFileName = "output"
    folder = "./"
    files = []

    def print(self):
        '''
        for debugging
        '''
        print("magnitude:", self.magnitude)
        print("depthMin:", self.depthMin)
        print("depthMax:", self.depthMax)
        print("deploymentP:", self.deploymentP)
        print("deploymentS:", self.deploymentS)
        print("position:", self.latitude1, self.longitude1, self.latitude2, self.longitude2)


class GroupOfEntries:
    '''
    object of all the lines considering one earthquake - using id as a identificator
    '''
    id = -1
    date = "unknown"
    time = "unknown"
    dateStruct = datetime.strptime("2000-1-1|20:20:20.1", "%Y-%m-%d|%H:%M:%S.%f")
    timeDifference = 0
    magnitude = -1
    latitude = -1
    longitude = -1
    coordinates = "unknown"
    depth = -1
    numberOfEntries = 0
    entriesP = 0
    entriesS = 0
    entries = ""

    def setNew(self, line):
        '''
        sets new object, works almost as constructor - is not contstructor for easier changing
        '''
        # becaouse of a different format of data in 2000nad5
        if 18 < len(line):
            line.pop(10)
        if 18 < len(line):
            line.pop(18)
        self.date = line[0]
        self.id = line[1]
        self.time = line[2]
        self.magnitude = line[11]
        self.coordinates = line[3] + line[4] + ' ' + line[5] + line[6]
        self.latitude = line[3]
        self.longitude = line[5]
        self.depth = line[8]
        #self.entries = '\t' + line[9] + ' ' + line[13] + ' ' + line[16] + '\n'
        self.numberOfEntries = 1
        if line[13] == "P":
            self.entriesP = self.entriesP + 1
        if line[13] == "S":
            self.entriesS = self.entriesS + 1
        self.dateStruct = datetime . strptime(self.date + "|" + self.time, "%Y-%m-%d|%H:%M:%S.%f")
        self.addNew(line)

    def addNew(self, line):
        '''
        adds new entry (one line in data files) considering the proper id
        '''
        # becaouse of a different format of data in 2000nad5
        if line[11] == "Ml=":
            line.pop(10)
        datetimeEnd = datetime.strptime(line[15] + "|" + line[16], "%Y-%m-%d|%H:%M:%S.%f")
        if datetimeEnd < self.dateStruct:
            print ("Warning: mistake in end time at entry", self.id)
        deltatime = datetimeEnd - self.dateStruct
        padding = ""
        for i in range(7 - len(line[9])):
            padding = padding + " "
        self.entries = self.entries + '\t' + ' ' + line[13] + ' ' + str(deltatime) + '\n'
        self.numberOfEntries = self.numberOfEntries + 1
        if line[13] == "P":
            self.entriesP = self.entriesP + 1
        if line[13] == "S":
            self.entriesS = self.entriesS + 1
'''
    Data read
'''
def mergeFiles(files, mypath):
    buffer = ""
    for f in files:
        try:
            filename = mypath + f
            fArray = f.split(".")
            if not fArray[-1] == 'bul':
                print("skipped: " + f)
                continue
            tmpfile = open(filename, mode='r', encoding='utf-8')
        except:
            print("Could not open this file: " + mypath + f)
            continue
        buffer = buffer + tmpfile . read()
        tmpfile . close()
    bufferArray = buffer . split("\n")
    parsedBuffer = []
    print(len(bufferArray))
    # parses all lines to array
    for line in bufferArray:
        line = " ".join(line.split())
        line = "".join(line.split(";"))
        lineArray = line . split(" ")
        parsedBuffer . append(lineArray)
    return parsedBuffer

def openOutputFile(name):
    '''
    open a file for the output - adds a number postfix if the previous one already exists
    :param name: file name
    :return: opened file
    '''
    postfix = ''
    count = 0
    while True:
        try:
            output = open(name + postfix + ".txt", "x")
        except:
            count = count + 1
            postfix = str(count)
            continue
        break
    return output


def isCorrectGroupOfEntries(configInfo, dataEntry):
    '''
    checks whether the data is relevat
    :param configInfo: configuration information
    :param dataEntry: the specific piece of data
    :return:
    '''
    try:
        if float(dataEntry . magnitude) < float(configInfo . magnitude):
            return False
        # warning - the newer files have a different type of depth saving
        if float(dataEntry . depth) < 100:
            newvalue = float(dataEntry.depth)*1000
            dataEntry.depth = str(newvalue)
        if float(dataEntry . depth) < float(configInfo . depthMin) or float(configInfo . depthMax) < float(dataEntry . depth):
            return False
        if float(dataEntry . entriesP) < float(configInfo . deploymentP) or float(dataEntry . entriesS) < float(configInfo . deploymentS):
            return False
        if float(dataEntry . latitude) < float(configInfo . latitude1) or float(configInfo . latitude2) < float(dataEntry . latitude):
            return False
        if float(dataEntry . longitude) < float(configInfo . longitude1) or float(configInfo . longitude2) < float(dataEntry . longitude):
            return False
        # space for more conditions
    except:
        return False
    return True


def filterEntries(buffer, configInfo):
    '''
    filters the whole set of data
    :param buffer: array of lines
    :param configInfo: configuration info used for selecting the correct data
    :return:
    '''
    correctEntries = []
    present = GroupOfEntries()
    present.setNew(buffer[0])
    buffer.pop(0)
    # cycle goes through the data and selects only the relevant ones
    for line in buffer:
        if len(line) < 18:  # skip not completed data
            continue
        if present.id == line[1]:
            present.addNew(line)
        else:
            if isCorrectGroupOfEntries(configInfo, present):
                correctEntries . append(present)
            present = GroupOfEntries()
            present . setNew(line)
    # checks wheter the group of entries fits the configuration properities
    if isCorrectGroupOfEntries(configInfo, present):
        correctEntries.append(present)
    # prints the output file
    output = openOutputFile(configInfo.outputFileName)
    for entry in correctEntries:
        padding = ""
        for i in range(9 - len(entry.id)):
            padding = padding + " "
        output.write(entry . id + padding + ' ' + entry . date + ' ' + entry . time + ' ' + entry . magnitude + ' ' +
                     entry . coordinates + ' ' + entry . depth + "\n")
        output.write(entry.entries)
    output.close()
    return


'''
    Initialization
'''
def main():
    configInfo = readConfigurationFile()
    if not configInfo:
        return False
    buffer = mergeFiles(configInfo.files, configInfo.folder)
    filterEntries(buffer, configInfo)


main()
