import os

criteriaFile = open("kriteria.txt", "w")

def readFloatFromUser(line):
    '''
    reads and sets a float from the user
    :param line: string what is supposed to be set
    :return: returns float from user
    '''
    while True:
        inputfloat = input(line)
        type(inputfloat)
        try:
            inputfloat = float(inputfloat )
        except:
            print("Prosím napiš číslo (desetinné)")
            continue
        return inputfloat

minimal_maginude = readFloatFromUser("Nastaveni minimalní magnitudy:\n")
coord = [None, None, None, None]
coord_questions = [
    "Nastav první souřadnici sever/jih (eg.: 50.1):\n",
    "Nastav druhou souřadnici východ/západ (eg.: 15.0):\n",
    "Nastav třetí souřadnici sever/jih (eg.: 50.1):\n",
    "Nastav čtvrtou souřadnici východ/západ (eg.: 15.1):\n"
]
for i in range(len(coord)):
    coord[i] = readFloatFromUser(coord_questions[i])

min_depth = readFloatFromUser("Nastav minimální hloubku\n")
max_depth = readFloatFromUser("Nastav maxinmální hloubku\n")
deploymentP = input("Nastav počet P nasazení\n")
deploymentS = input("Nastav počet S nasazení\n")
output_file = input("Nastav název výstupního souboru\n")
type(output_file)
folder = input("Nastav název adresáře s daty\n")
type(folder)
input_files = input("Vypiš soubory z adresáře, které chceš nastavit, jako vstupní.\n"
                    "Pro nastavení všech souborů napiš 'all' (vybere všechny soubory s koncovkou '.bul'\n")
type(input_files)

criteriaFile . write(
    str(minimal_maginude) + " minimalni magnituda\n" +
    str(coord[0])+ " " + str(coord[1]) + " " + str(coord[2]) + " " + str(coord[3]) + " ctverec\n" +
    str(min_depth) + " " + str(max_depth) + " hloubka\n" +
    str(deploymentP) + " P nasazeni\n" +
    str(deploymentS) + " S nasazeni\n" +
    output_file + " vystup\n" +
    folder + " adresar\n" +
    input_files
)

criteriaFile . close()

