from os import walk
import os



class ConfigurationInfo:
    '''
    object of configuration information
    '''
    magnitude = "undefined"
    depthMin = -1
    depthMax = -1
    latitude1 = -1
    longitude1 = -1
    latitude2 = -1
    longitude2 = -1
    deploymentP = -1
    deploymentS = -1
    outputFileName = "output"
    folder = "./"
    files = []

    def print(self):
        '''
        for debugging
        '''
        print("magnitude:", self.magnitude)
        print("depthMin:", self.depthMin)
        print("depthMax:", self.depthMax)
        print("deploymentP:", self.deploymentP)
        print("deploymentS:", self.deploymentS)
        print("position:", self.latitude1, self.longitude1, self.latitude2, self.longitude2)


def readConfigurationFile():
    '''
    reads the configuration file and checks whether it is correct
    :return: false if mistake otherwise ConfigurationInfo object with proper values
    '''
    try:
        file = open('kriteria.txt', mode='r', encoding='utf-8')
    except:
        print("Create criteria file called 'kriteria.txt' ")
        return False
    print("Reading Magnitude")
    configInfo = ConfigurationInfo()
    configInfo . magnitude = magnitudeCheck(file.readline())
    print(configInfo . magnitude)
    if not isinstance(configInfo.magnitude, float):
        return False
    print("Reading Position")
    positionArray = positionCheck(file.readline())
    if not positionArray:
        return False
    else:
        configInfo.latitude1 = positionArray[0]
        configInfo.longitude1 = positionArray[1]
        configInfo.latitude2 = positionArray[2]
        configInfo.longitude2 = positionArray[3]
    print("Reading Depth")
    depthArray = depthCheck(file.readline())
    if not depthArray:
        return False
    else:
        configInfo . depthMin = depthArray[0]
        configInfo . depthMax = depthArray[1]
    print("Reading P waves")
    configInfo . deploymentP = numberOfPrimaryWaves(file.readline())
    if not configInfo . deploymentP:
        return False
    print("Reading S waves")
    configInfo . deploymentS = numberOfSecondaryWaves(file.readline())
    if not configInfo . deploymentS:
        return False
    print("Reading output file requirements")
    configInfo.outputFileName = outputFileName(file.readline())
    if not configInfo.outputFileName:
        return False
    print("Reading folder information")
    configInfo.folder = folderCheck(file.readline())
    if not configInfo.folder:
        return False
    print("Reading files")
    configInfo.files = filesCheck(file.readline(), configInfo.folder)
    if not configInfo.files:
        return False
    print(configInfo.files)
    file . close()
    return configInfo


def magnitudeCheck(line):
    '''
    checks the magnitude of an entry
    :param line: line in configuration file
    :return: false if wrong text otherwise the minimal magnitude
    '''
    line = ("").join(line.split("\n")) # gets rid of the new line
    lineList = line.split(" ")
    if len(lineList) == 3 and lineList[1] == "minimalni" and lineList[2] == "magnituda":
        try:
            minMagnitude = float(lineList[0])
        except:
            print("Please write a number in the first line."
                  "Format: 0.444 minimal magnituda")
            return False
    else:
        print("Cannot read the first line of the Configuration file. "
              "Format: 0.444 minimal magnituda")
        return False
    return minMagnitude


def positionCheck(line):
    '''
    checks the position of an entry
    :param line: line in configuration file
    :return: false if wrong text otherwise the list of square from coordinates
    '''
    line = ("").join(line.split("\n"))  # gets rid of the new line
    lineList = line.split(" ")
    if len(lineList) == 5 and lineList[4] == "ctverec":
        try:
            latitude1 = lineList[0]
            longitude1 = lineList[1]
            latitude2 = lineList[2]
            longitude2 = lineList[3]
            # makes sure that the first latitude and longitude is lower
            if latitude2 < latitude1:
                latitude1, latitude2 = latitude2, latitude1
            if longitude2 < longitude1:
                longitude1, longitude2 = longitude2, longitude1
            positionArray = [latitude1, longitude1, latitude2, longitude2]
        except:
            print("Please write a numbers in the second line."
                  "Format: 50.5 12.0 51.4 12.6 Ctverec (latitude longitude latitude longitude ctverec)")
            return False
    else:
        print("Cannot read the second line of the Configuration file. "
              "Format: 50.5 12.0 51.4 12.6 Ctverec (latitude longitude latitude longitude ctverec)")
        return False
    return positionArray


def depthCheck(line):
    '''
    checks the depth of an entry
    :param line: line in configuration file
    :return: false if wrong text otherwise the value of minimal depth
    '''
    line = ("").join(line.split("\n"))  # gets rid of the new line
    lineList = line.split(" ")
    if len(lineList) == 3 and lineList[2] == "hloubka":
        try:
            minDepth = lineList[0]
            maxDepth = lineList[1]
        except:
            print("Please write a numbers in the third line."
                  "Format: 7000 10000 hloubka (MinimalDepth MaximalDepth hloubka)")
            return False
    else:
        print("Cannot read the third line of the Configuration file. "
              "Format: 7000 10000 hloubka (MinimalDepth MaximalDepth hloubka)")
        return False
    return [minDepth, maxDepth]


# checks the number of Primary entries
def numberOfPrimaryWaves(line):
    '''
    checks the numer of primary etries
    :param line: line in configuration file
    :return: false if wrong text otherwise the value of P waves
    '''
    line = ("").join(line.split("\n"))  # gets rid of the new line
    lineList = line.split(" ")
    if len(lineList) == 3 and lineList[1] == "P" and lineList[2] == "nasazeni":
        try:
            deploymentP = lineList[0]
        except:
            print("Please write a numbers in the fourth line."
                  "Format: 8 P nasazeni")
            return False
    else:
        print("Cannot read the fourth line of the Configuration file. "
              "Format: 8 P nasazeni")
        return False
    return deploymentP


def numberOfSecondaryWaves(line):
    '''
        checks the numer of primary etries
        :param line: line in configuration file
        :return: false if wrong text otherwise the value of P waves
        '''
    line = ("").join(line.split("\n"))  # gets rid of the new line
    lineList = line.split(" ")
    if len(lineList) == 3 and lineList[1] == "S" and lineList[2] == "nasazeni":
        try:
            deploymentS = lineList[0]
        except:
            print("Please write a numbers in the fifth line."
                  "Format: 6 S nasazeni")
            return False
    else:
        print("Cannot read the fifth line of the Configuration file. "
              "Format: 6 S nasazeni")
        return False
    return deploymentS


def outputFileName(line):
    line = ("").join(line.split("\n"))  # gets rid of the new line
    lineList = line.split(" ")
    if len(lineList) == 2 and lineList[1] == "vystup":
        fileName = lineList[0]
    else:
        print("Cannot read the sixth line of the Configuration file. "
              "Format: outputFile vystup")
        return False
    return fileName


def folderCheck(line):
    '''
           checks the line with folder information
           :param line: line in configuration file
           :return: false if wrong text otherwise the folder with proper data
    '''
    line = ("").join(line.split("\n"))  # gets rid of the new line
    lineList = line.split(" ")
    if len(lineList) == 2 and lineList[1] == "adresar" and os.path.isdir(lineList[0]) :
        folder = lineList[0]
        lastChar = folder[-1]
        if lastChar != '/':
            folder = folder + '/'
    else:
        print("Cannot read the seventh line of the Configuration file. "
            "Format: ./odecty_WEBNET_REYKJANET adresar")
        return False
    return folder


def filesCheck(line, mypath):
    '''
        reads all the data files whose the program will be working with
        :param line: line in configuration file
        :return: false if no files given otherwise the list of files
    '''
    line = ("").join(line.split("\n"))  # gets rid of the new line
    lineList = line.split(" ")
    if not len(lineList) == 0:
        files = []
        # reads all files from path directory
        if lineList[0] == "all":
            for (dirpath, dirnames, filenames) in walk(mypath):
                files.extend(filenames)
                break
        else:
            # if postfix '.bul' is missing add it there
            for f in lineList:
                fArray = f.split(".")
                if not fArray[-1] == 'bul':
                    f = f + '.bul'
                files . append(f)
    else:
        print("Cannot read the eight line of the Configuration file. "
              "Format: file1 file2 file3 ..."
              "Format: all (reads all the files in the given folder)")
        return False
    return files
