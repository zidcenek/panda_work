odecty.py
- hlavní python soubor ke spuštění
- načítá konfiguraci ze souboru kriteria.txt
- projde všechny definované soubory v kriteria.txt ve složce definované v kriteria.txt
- v příkazové řádce spustit pomocí: py odecty.py

criteria.py
- kontroluje korektnost souboru kriteria.txt
- není potřeba zpouštět spouští ho odecty.py

generateCriteria.py
- pomáhá generovat konfigurační soubor kriteria.py
kriteria.txt
- lze bez problému upravovat, pozor na syntaxi, při špatné nepůjde spusit, program upozorní, kde je chyba

kriteria.txt
- předposlední řádek - nastavuje adresář, ze kterého čerpá vstupní soubory
- poslední řádek nastavuje soubory, na vstup: akceptované vstupy
    - 2001, 2002, 2003
    - 2001.bul, 2002.bul
    - all (vybere všechny .bul soubory ve složce)

